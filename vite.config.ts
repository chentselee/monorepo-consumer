import { defineConfig } from 'vite'
import reactRefresh from '@vitejs/plugin-react-refresh'

const isGitlab = process.env.IS_GITLAB === 'true'

export default defineConfig({
  base: isGitlab ? '/monorepo-consumer/' : '.',
  plugins: [reactRefresh()],
})
