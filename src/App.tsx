import React from 'react'
import { Button } from '@mylib/button'
import { Heading1 } from '@mylib/typography'

function App() {
  return (
    <div className='App'>
      <Heading1>Monorepo Consumer</Heading1>
      <Button>custom button</Button>
    </div>
  )
}

export default App
